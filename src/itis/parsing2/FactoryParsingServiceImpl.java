package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    private String removeQuotes(String string) {
        return string.trim().replaceAll("\"", "");

    }

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {

        try {

            HashMap<String, String> map = new HashMap<>();
            BufferedReader br = new BufferedReader(new FileReader(factoryDataDirectoryPath));
            br.readLine();
            String line;

            while (!(line = br.readLine()).trim().equals("---")) {
                String[] arr = (line + " ").split(":");
                map.put(removeQuotes(arr[0]), removeQuotes(arr[1]));
            }

            return factoryDataFromMap(map);

        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new FactoryParsingException("Failed to set Park from data file", new ArrayList<>());
        }
    }

    private Factory factoryDataFromMap(HashMap<String, String> map) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        Class<Factory> factoryClass = Factory.class;
        Constructor<Factory> constructor = factoryClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Factory factoryInstance = constructor.newInstance();

        List<FactoryParsingException.FactoryValidationError> errors = new ArrayList<>();
        List<String> messages = new ArrayList<>();

        for (Field f : factoryClass.getDeclaredFields()) {

            f.setAccessible(true);
            Annotation[] annotations = f.getAnnotations();

            for (Annotation a : annotations) {

                if (a.annotationType() == NotBlank.class
                        && (!map.containsKey(f.getName()) || removeQuotes(map.get(f.getName())).isEmpty())) {
                    String errorMessage = "Field " + f.getName() + " cannot be empty";
                    messages.add(errorMessage);
                    errors.add(new FactoryParsingException.FactoryValidationError(f.getName(), errorMessage));
                }

                if (a.annotationType() == Concatenate.class) {
                    List<String> values = Arrays.stream(((Concatenate) a).fieldNames()).map(s -> map.getOrDefault(s, "")).collect(Collectors.toList());
                    f.set(factoryInstance, String.join(((Concatenate) a).delimiter().trim(), values));
                }
            }

            if (f.get(factoryInstance) == null && map.containsKey(f.getName())) {
                f.set(factoryInstance, returnObject(f.getType(), map.get(f.getName())));
            }
        }

        if (errors.isEmpty()) {
            return factoryInstance;
        } else {
            throw new FactoryParsingException(messages.size() + " errors were found", errors);
        }
    }

    private Object returnObject(Class parentClass, String data) {

        if (data.equals("null")) {
            return null;
        } else if (parentClass.isAssignableFrom(Long.class)) {
            return Long.parseLong(data);
        } else if (parentClass.isAssignableFrom(List.class)) {

            return Arrays.stream(data.substring(1, data.length() - 1).split(",")).map(this::removeQuotes).collect(Collectors.toList());
        }

        return data;
    }

}