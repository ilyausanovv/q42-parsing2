package itis.parsing2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

interface FactoryParsingService {

    Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException, IOException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

}
